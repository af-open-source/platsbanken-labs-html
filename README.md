# Prototype for Platsbanken Labs app
>This prototype was created to test concepts and functionality for Platsbanken Labs iOS app

**[beta.platzbanken.se](http://beta.platzbanken.se)** (open in your mobile)

## Install
Make sure you have Node.js and npm installed.

```bash
$ git clone https://simon-johansson@bitbucket.org/afappar/platsbanken-labs-html.git && cd platsbanken-labs-html
$ npm install
```

## Development
To run the prototype locally, watch for file changes and compile as you go:
```bash
$ npm start
```
The site will be available on `localhost:3000`

## Deployment
The site is configured to be deployed to [Heroku](http://heroku.com)
