FROM node:6.9
COPY . .
EXPOSE 3000
RUN npm install
RUN npm run build:dll
CMD npm run start
